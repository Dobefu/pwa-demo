<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#888">

    <title>PWA demo</title>

    <link rel="stylesheet" href="/css/style.css">
    <link rel="manifest" href="/manifest.json">
  </head>
  <body>
    <h1>PWA</h1>

    <script src="https://www.gstatic.com/firebasejs/5.4.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.4.2/firebase-messaging.js"></script>

    <script src="/js/service-workers/main.js"></script>
  </body>
</html>
