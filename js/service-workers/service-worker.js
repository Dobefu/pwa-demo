// Import the Firebase scripts.
importScripts('https://www.gstatic.com/firebasejs/5.4.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.4.2/firebase-messaging.js');

self.addEventListener('install', function() {
  console.info('[ServiceWorker] Installing');
});

self.addEventListener('fetch', function() {
  console.info('[ServiceWorker] Fetching');
});

self.addEventListener('activate', function() {
  console.info('[ServiceWorker] Activating');
});

let config = {
  apiKey: "API_KEY",
  authDomain: "AUTH_DOMAIN",
  databaseURL: "DATABASE_URL",
  projectId: "PROJECT_ID",
  storageBucket: "STORAGE_BUCKET",
  messagingSenderId: "MESSAGING_SENDER_ID"
};

firebase.initializeApp(config);
const messaging = firebase.messaging();

/**
 * Listens for messages in the background, even when the browser is closed.
 *
 * @param {object} payload
 *   The payload sent to the service worker.
 */
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[ServiceWorker] Received background message ', payload);

  let message = payload.data;

  let title = message.title;
  let options = {
    body: message.body,
    click_action: message.click_action,
    icon: '/img/512x512.png',
    badge: '/img/192x192.png'
  };

  return self.registration.showNotification(title, options);
});
