let config = {
  apiKey: "API_KEY",
  authDomain: "AUTH_DOMAIN",
  databaseURL: "DATABASE_URL",
  projectId: "PROJECT_ID",
  storageBucket: "STORAGE_BUCKET",
  messagingSenderId: "MESSAGING_SENDER_ID"
};

firebase.initializeApp(config);
const messaging = firebase.messaging();

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {

    // Register the service worker and set the scope to /.
    navigator.serviceWorker.register('/js/service-workers/service-worker.js', { scope: '/' }).then (function(reg) {
      console.info('[ServiceWorker] Registered');
      initFirebase(reg);
    },
    function(error) {
      console.error('[ServiceWorker] Registration failed: ', error);
    });

    if (navigator.onLine === false) {
      document.getElementsByTagName('h1')[0].textContent = "Offline!";
    }
  });
}

/**
 * Tries to initialize Firebase for messaging.
 *
 * @param {object} registration
 *   The service worker registration.
 */
function initFirebase(registration) {
  // Use the running service worker for Firebase messaging.
  messaging.useServiceWorker(registration);

  // Use the public key from Firebase as the VAPID key.
  messaging.usePublicVapidKey(
    "PUBLIC_VAPID_KEY"
  );

  messaging.requestPermission().then(function () {
    console.info('[ServiceWorker] Notification permission granted.');
    getToken();
  })
  .catch(function (err) {
    console.error('[ServiceWorker] Unable to get permission to notify.', err);
  });
}

/**
 * Listens for messages while the page is focused.
 *
 * @param {object} payload
 *   The payload sent to the client.
 *
 * @return {boolean|event}
 *   Return the message if the page has focus.
 */
messaging.onMessage(function(payload) {
  if (document.hasFocus() === false) {
    return false;
  }

  console.info('[ServiceWorker] Received background message ', payload);

  let message = payload.data;

  let title = message.title;
  let options = {
    body: message.body,
    click_action: message.click_action,
    icon: '/img/512x512.png',
    badge: '/img/192x192.png'
  };

  return self.registration.showNotification(title, options);
});

/**
 * Tries to get a token from the server. Requests a new one if there is no token.
 */
function getToken() {
  messaging.getToken().then(function (currentToken) {
    console.info('[ServiceWorker] Token is ' + currentToken);

    if (currentToken) {
      // TODO: send token to server.
    } else {
      console.warn('[ServiceWorker] No token available. Requesting permission to generate one.');
      // TODO: request new token from server.
    }
  })
  .catch(function (err) {
    console.error('[ServiceWorker] Error retrieving token. ', err);
  });
}